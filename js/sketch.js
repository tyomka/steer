/*
| DRAWING
|
| THE SOURCE - https://www.youtube.com/watch?v=4hA7G3gup-4
*/

let canvas;

let xcore;
let ycore;

let vehicles;

let font;
let aword = "tyomka";
let pos;
let asize;

function preload() {	
	font = loadFont("assets/ITCSerifGothic.otf");
}

function setup() {
    canvas = createCanvas(document.body.clientWidth, document.body.clientHeight);
    canvas.parent("sketch");
	
    colorMode(HSB, 360);
	background(0);

    xcore = width / 2;
    ycore = height / 2;
	
	pos = createVector(xcore - (aword.length * 50), ycore);
	asize = 192;
	
	let points = font.textToPoints(aword, pos.x, pos.y, asize);
	
	vehicles = [];
	for (let pt of points){
		vehicles.push(Vehicle(pt.x, pt.y));
	}
}

function draw() {
    background(0);
	
	fill(255, 8);	
	noStroke();
	textFont(font);
	textSize(asize);
	text(aword, pos.x, pos.y);
	
	for (let vehicle of vehicles){
		vehicle.behaviors(mouseDown);
		vehicle.update();
		vehicle.show();
	}
}

const define = function(word){ 
	aword = word;
	
	pos = createVector(xcore - (aword.length * 50), ycore);
	let points = font.textToPoints(word, pos.x, pos.y, asize);
	
	vehicles = [];
	for (let pt of points) vehicles.push(Vehicle(pt.x, pt.y));
};

/*
| FORM EVENTS
*/

let mouseDown = false;
document.addEventListener("mousedown", function(){ mouseDown = true; });
document.addEventListener("mouseup", function(){ mouseDown = false; });
